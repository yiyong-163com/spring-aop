package com.springframework.aop.test;

import com.springframework.aop.aspectj.AspectJExpressionPointcut;
import com.springframework.aop.fremework.Cglib2AopProxy;
import com.springframework.aop.fremework.JdkDynamicAopProxy;
import com.springframework.aop.pojo.TargetSource;
import com.springframework.aop.service.AdvisedSupport;
import com.springframework.aop.test.dao.IUserService;
import com.springframework.aop.test.interceptor.UserServiceInterceptor;
import com.springframework.aop.test.service.UserDataService;
import com.springframework.aop.test.service.UserService;

import java.lang.reflect.Method;

public class TestAopExample {
    public static void main(String[] args) {
        //test_aop();
        test_aopProxy();
    }

    public  static void test_aop(){
        AspectJExpressionPointcut  aspectJExpressionPointcut = new AspectJExpressionPointcut("execution(* com.springframework.aop.test.service.UserService.*(..))");
        Class<UserService>  userServiceClass =    UserService.class;
        try {
            Method method =  userServiceClass.getDeclaredMethod("queryUserInfo");
            System.out.printf("匹配:"+aspectJExpressionPointcut.matches(userServiceClass));
            System.out.printf("匹配:"+aspectJExpressionPointcut.matches(method,userServiceClass));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    public  static void test_aopProxy(){
        IUserService  userService = new UserDataService();
        //// 组装代理信息
        AdvisedSupport advisedSupport =        new AdvisedSupport();
        advisedSupport.setTargetSource(new TargetSource(userService));
        advisedSupport.setMethodMatcher(new AspectJExpressionPointcut("execution(* com.springframework.aop.test.dao.IUserService.*(..))"));
        advisedSupport.setMethodInterceptor(new UserServiceInterceptor());
        // 代理对象(JdkDynamicAopProxy)

    //    IUserService   jdkDynamicAopProxy = (IUserService) new JdkDynamicAopProxy(advisedSupport).getProxy();
        // 测试调用
    //    System.out.println("测试结果：" + jdkDynamicAopProxy.queryUserInfo());

        IUserService  cglib2AopProxy = (IUserService) new Cglib2AopProxy(advisedSupport).getProxy();
        // 测试调用
        System.out.println("测试结果：" + cglib2AopProxy.queryUserInfo());


    }
}
