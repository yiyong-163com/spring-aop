package com.springframework.aop.test.dao;

public interface IUserDao {
    String queryUserName(String uId);
}
