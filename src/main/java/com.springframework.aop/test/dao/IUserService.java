package com.springframework.aop.test.dao;

public interface IUserService {
    public String queryUserInfo();
    public String register(String userName);
}
