package com.springframework.aop.service;

public interface ClassFilter {
    /**
     * 匹配对应的信息Class
     * @param clazz
     * @return
     */
    boolean matches(Class<?>  clazz);
}
